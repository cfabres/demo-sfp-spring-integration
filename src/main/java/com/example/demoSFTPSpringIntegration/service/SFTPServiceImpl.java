package com.example.demoSFTPSpringIntegration.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpSession;
import org.springframework.stereotype.Service;

import com.jcraft.jsch.ChannelSftp.LsEntry;

@Service
public class SFTPServiceImpl {

	@Autowired
	private DefaultSftpSessionFactory defaultSftpSessionFactory;
	
	/**
	 * list all files and folders for some specific path
	 * example path:
	 * path=./.composer
	 * path=.
	 * 
	 * @param path
	 * @return
	 */
	public List<LsEntry> list(String path) {
		SftpSession session = this.defaultSftpSessionFactory.getSession();
		LsEntry[] lsEntry=null;
		try {
			lsEntry = session.list(path);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}			
		}
		return new ArrayList<LsEntry>(Arrays.asList(lsEntry));
	}

	/**
	 * copy a file from SFTP to Local
	 * If the file exists locally, will be replaced
	 * 
	 * @param remotePath
	 * @param localPath
	 * @return boolean
	 */
	public boolean download(String remotePath, String localPath) {
		boolean state = false;
		SftpSession session = this.defaultSftpSessionFactory.getSession();
		InputStream remoteStream = null;
		try {
			remoteStream = session.readRaw(remotePath);
			File localFile = new File(localPath);
			java.nio.file.Files.copy(
				      remoteStream, 
				      localFile.toPath(), 
				      StandardCopyOption.REPLACE_EXISTING);
		    IOUtils.closeQuietly(remoteStream);
		    state = true;
		} catch (IOException e) {
			e.printStackTrace();
			state = false;
		} finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}			
		}
		return state;
	}
	
	/**
	 * delete a file in the SFTP server
	 * returns a boolean is the operation was success or not
	 * 
	 * @param path
	 * @return
	 */
	public boolean delete(String path) {
		boolean state = false;
		SftpSession session = this.defaultSftpSessionFactory.getSession();
		InputStream remoteStream = null;
		try {
			state = session.remove(path);
		} catch (IOException e) {
			e.printStackTrace();
			state = false;
		} finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}			
		}
		return state;
	}

	/**
	 * upload a file from local to SFTP
	 * return boolean if the operation was success or not 
	 * 
	 * @param remotePath
	 * @param localPath
	 * @return
	 */
	public boolean upload(String remotePath, String localPath) {
		boolean state = false;
		SftpSession session = this.defaultSftpSessionFactory.getSession();
		try {
			
			File initialFile = new File(localPath);
		    InputStream inputStream = FileUtils.openInputStream(initialFile);
			session.append(inputStream, remotePath);
			IOUtils.closeQuietly(inputStream);
		    state = true;
		} catch (IOException e) {
			e.printStackTrace();
			state = false;
		} finally {
			if (session!=null && session.isOpen()) {
				session.close();
			}			
		}
		return state;
	}
}