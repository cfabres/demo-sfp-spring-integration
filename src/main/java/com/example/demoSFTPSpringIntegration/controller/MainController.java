package com.example.demoSFTPSpringIntegration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoSFTPSpringIntegration.service.SFTPServiceImpl;
import com.jcraft.jsch.ChannelSftp.LsEntry;

@RestController
public class MainController {

	@Autowired
	private SFTPServiceImpl sftp;
	
	/**
	 * Controller to handle the list of the file in some specific path, filling out the path request parameter.
	 * 
	 * example 1: http://localhost:8080/list?path=.
	 * example 2: http://localhost:8080/list?path=./.composer
	 * 
	 * 
	 * @param path
	 * @return
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public List<LsEntry> listDirectory(@RequestParam String path) {
		return this.sftp.list(path);
	}
	
	/**
	 * The method will download a file from SFTP to local file system
	 * there are two parameters, remotePAth and localPath 
	 * If the download is successful, will return true, in any other case will return false
	 * If the file locally exists, will replace it.
	 * 
	 * example 1: http://localhost:8080/download?remotePath=/home/aplicaciones/paquete_movilidad_1412.zip&localPath=c:/paquete_1412.zip
	 * both path exists.
	 * failed with access denied -> returned false
	 * 
	 * example 2: http://localhost:8080/download?remotePath=/home/aplicaciones/paquete_movilidad_1412.zip&localPath=C:/Users/User  user/Downloads/paquete_1412.zip
	 * both path exists.
	 * returned true
	 * 
	 * example 3: http://localhost:8080/download?remotePath=/home/phpservices/test.php&localPath=C:/Users/User  user/Downloads/test.php
	 * example 4: http://localhost:8080/download?remotePath=/home/myfolder/AWS.JPG&localPath=C:/Users/User  user/Downloads/AWS.JPG
	 * example 5: http://localhost:8080/download?remotePath=/home/myfolder/ejemplo.docx&localPath=C:/Users/User  user/Downloads/ejemplo.docx
	 * example 6: http://localhost:8080/download?remotePath=/home/myfolder/Spring.JPG&localPath=C:/Users/User  user/Downloads/Spring.JPG
	 * both path exists.
	 * returned true
	 * 
	 * @param remotePath
	 * @param localPath
	 * @return
	 */
	@RequestMapping(value="/download",method=RequestMethod.GET)
	public Boolean download(@RequestParam String remotePath, @RequestParam String localPath) {
		return this.sftp.download(remotePath, localPath);
	}
	
	/**
	 * delete a file in the SFTP server
	 * return boolean if the operation was success or not
	 * 
	 * example 1: http://localhost:8080/delete?path=/home/myfolder/xxx.yyy
	 * returns false: the path doesn't exist.
	 * 
	 * example 2: http://localhost:8080/delete?path=/home/myfolder/AWS.JPG
	 * example 3: http://localhost:8080/delete?path=/home/myfolder/ejemplo.docx
	 * example 4: http://localhost:8080/delete?path=/home/myfolder/Spring.JPG
	 * returns true
	 * 
	 * @param path
	 * @return
	 */
	@RequestMapping(value="/delete",method=RequestMethod.GET)
	public Boolean delete(@RequestParam String path) {
		return this.sftp.delete(path);
	}
	
	/**
	 * upload a file from local to remote.
	 * return boolean if the operation was success or not.
	 * 
	 * 
	 * example 1: http://localhost:8080/upload?localPath=C:/Users/User  user/Desktop/AWS.JPG&remotePath=/home/myfolder/AWS.JPG
	 * example 2: http://localhost:8080/upload?localPath=C:/Users/User  user/Desktop/ejemplo.docx&remotePath=/home/myfolder/ejemplo.docx
	 * example 3: http://localhost:8080/upload?localPath=C:/Users/User  user/Desktop/Spring.JPG&remotePath=/home/myfolder/Spring.JPG
	 * returns true
	 * 
	 * 
	 * @param remotePath
	 * @param localPath
	 * @return
	 */
	@RequestMapping(value="/upload",method=RequestMethod.GET)
	public Boolean upload(@RequestParam String remotePath, @RequestParam String localPath) {
		return this.sftp.upload(remotePath, localPath);
	}
	
}